import 'package:weather_app_state_rebuilder/models/weather.dart';

abstract class WeatherRepository {
  Future<Weather> fetchWeather(String cityName);

  Future<Weather> fetchDetailedWeather(Weather weather);
}
