import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:json_annotation/json_annotation.dart';

part 'weather.g.dart';

@JsonSerializable()
class Weather extends Equatable {
  final String cityName;
  final double temperature;
  final double temperatureFahrenheit;

  Weather({
    @required this.cityName,
    @required this.temperature,
    this.temperatureFahrenheit,
  });

  @override
  List<Object> get props => [cityName, temperature, temperatureFahrenheit];

  factory Weather.fromJson(Map<String, dynamic> json) =>
      _$WeatherFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherToJson(this);
}
