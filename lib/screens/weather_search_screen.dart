import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:weather_app_state_rebuilder/models/weather.dart';
import 'package:weather_app_state_rebuilder/repository/weather_repo_impl.dart';
import 'package:weather_app_state_rebuilder/state/weather_store.dart';
import 'package:weather_app_state_rebuilder/widgets/city_input_field.dart';
import 'package:weather_app_state_rebuilder/widgets/common_widgets.dart';

import '../router.gr.dart';

class WeatherSearchScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _WeatherSearchScreenState();
  }
}

class _WeatherSearchScreenState extends State<WeatherSearchScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Weather Search'),
      ),
      body: Container(
        padding: EdgeInsets.all(16.0),
        alignment: Alignment.center,
        child: StateBuilder<WeatherStore>(
          observe: () => RM.get<WeatherStore>(),
          builder: (context, reactiveModel) {
            return reactiveModel.whenConnectionState(
              onIdle: () => buildInitialInput(CityInputField(
                onSubmit: _submitWeather,
              )),
              onWaiting: () => buildLoading(),
              onData: (store) => buildColumnWithData(store.weather),
              onError: (_) => buildInitialInput(CityInputField(
                onSubmit: _submitWeather,
              )),
            );
          },
        ),
      ),
    );
  }

  Widget buildColumnWithData(Weather weather) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          weather.cityName,
          style: TextStyle(
            fontSize: 40,
            fontWeight: FontWeight.w700,
          ),
        ),
        Text(
          "${weather.temperature.toStringAsFixed(1)} °C",
          style: TextStyle(fontSize: 80),
        ),
        CityInputField(
          onSubmit: _submitWeather,
        ),
        RaisedButton(
          child: Text('See Details'),
          color: Colors.lightBlue[100],
          onPressed: () {
            ExtendedNavigator.root.pushNamed(
              Routes.weatherDetailsScreen,
              arguments: WeatherDetailsScreenArguments(masterWeather: weather),
            );
          },
        )
      ],
    );
  }

  void _submitWeather(String cityName) {
    final reactiveModel = Injector.getAsReactive<WeatherStore>();
    reactiveModel.setState(
      (store) => store.getWeather(cityName),
      onError: (context, error) {
        if (error is NetworkError) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text("Couldn't fetch weather. Is the device online?"),
            ),
          );
        } else {
          throw error;
        }
      },
    );
  }
}
