import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:weather_app_state_rebuilder/models/weather.dart';
import 'package:weather_app_state_rebuilder/state/weather_store.dart';
import 'package:weather_app_state_rebuilder/widgets/common_widgets.dart';

class WeatherDetailsScreen extends StatefulWidget {
  final Weather masterWeather;

  WeatherDetailsScreen({
    @required this.masterWeather,
  });

  @override
  State<StatefulWidget> createState() {
    return WeatherDetailsScreenState();
  }
}

class WeatherDetailsScreenState extends State<WeatherDetailsScreen> {
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final reactiveModel = Injector.getAsReactive<WeatherStore>();
    reactiveModel
        .setState((store) => store.getWeatherDetails(widget.masterWeather));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Weather Details"),
      ),
      body: StateBuilder<WeatherStore>(
          observe: () => RM.get<WeatherStore>(),
          builder: (context, reactiveModel) {
            return reactiveModel.whenConnectionState(
                onIdle: () => buildColumnWithData(widget.masterWeather),
                onWaiting: () => buildLoading(),
                onData: (store) => buildColumnWithData(store.weather),
                onError: (_) => buildColumnWithData(widget.masterWeather));
          }),
    );
  }

  Widget buildColumnWithData(Weather weather) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            weather.cityName,
            style: TextStyle(
              fontSize: 40,
              fontWeight: FontWeight.w700,
            ),
          ),
          Text(
            "${weather.temperature.toStringAsFixed(1)} °C",
            style: TextStyle(fontSize: 80),
          ),
          Text(
            "${weather.temperatureFahrenheit?.toStringAsFixed(1)} °F",
            style: TextStyle(fontSize: 80),
          ),
        ],
      ),
    );
  }
}
