import 'package:flutter/material.dart';


Widget buildInitialInput(Widget initialWidget) {
  return Center(
    child: initialWidget,
  );
}

Widget buildLoading() {
  return Center(
    child: const CircularProgressIndicator(),
  );
}