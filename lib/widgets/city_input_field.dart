import 'package:flutter/material.dart';

class CityInputField extends StatefulWidget {
  final ValueChanged<String> onSubmit;

  CityInputField({@required this.onSubmit});

  @override
  State<StatefulWidget> createState() {
    return CityInputFiledState();
  }
}

class CityInputFiledState extends State<CityInputField> {
  @override
  Widget build(BuildContext context) {
    return TextField(
      onSubmitted: widget.onSubmit,
      textInputAction: TextInputAction.search,
      decoration: InputDecoration(
        hintText: 'Enter City Name',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(12)),
        suffixIcon: Icon(Icons.search),
      ),
    );
  }
}
