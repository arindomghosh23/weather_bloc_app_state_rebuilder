import 'package:weather_app_state_rebuilder/models/weather.dart';
import 'package:weather_app_state_rebuilder/repository/weather_repository.dart';

class WeatherStore {
  final WeatherRepository _repository;

  WeatherStore(this._repository);

  Weather _weather;

  Weather get weather => _weather;

  void getWeather(String cityName) async {
    _weather = await _repository.fetchWeather(cityName);
  }

  void getWeatherDetails(Weather weather) async {
    _weather = await _repository.fetchDetailedWeather(weather);
  }
}
