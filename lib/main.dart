import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:weather_app_state_rebuilder/repository/weather_repo_impl.dart';
import 'package:weather_app_state_rebuilder/router.gr.dart';
import 'package:weather_app_state_rebuilder/state/weather_store.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Injector(
        inject: [
          Inject<WeatherStore>(() => WeatherStore(WeatherRepositoryImpl()))
        ],
        builder: (_) {
          return MaterialApp(
            title: 'Flutter Demo',
            theme: ThemeData(
              primarySwatch: Colors.blue,
              visualDensity: VisualDensity.adaptivePlatformDensity,
            ),
            builder: ExtendedNavigator<Router>(
              router: Router(),
            ),
          );
        }

    );
  }
}
